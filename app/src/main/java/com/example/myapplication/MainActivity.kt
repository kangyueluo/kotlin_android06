package com.example.myapplication

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val TAG : String ="MainActivity"
    private val ACTION_CAMERA_REQUEST_CODE = 100
    private val ACTION_ALBUM_REQUEST_CODE = 200


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        imageView.scaleType = ImageView.ScaleType.CENTER_CROP

        cameraAppButton.setOnClickListener(View.OnClickListener {
            Log.d(TAG,"take image from camera")

            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent,ACTION_CAMERA_REQUEST_CODE)

        })

        albumAppButton.setOnClickListener(View.OnClickListener {
            Log.d(TAG,"take image from album")

            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent,ACTION_ALBUM_REQUEST_CODE)

        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        Log.d(TAG,"收到："+requestCode)

        when(requestCode){
            ACTION_CAMERA_REQUEST_CODE ->{
                if(resultCode == Activity.RESULT_OK && data != null){
                    displayImage(data.extras.get("data")as Bitmap)
                }
            }

            ACTION_ALBUM_REQUEST_CODE ->{
                if(resultCode == Activity.RESULT_OK && data != null){
                    val resolver = this.contentResolver
                    val bitmap = MediaStore.Images.Media.getBitmap(resolver,data.data)
                    displayImage(bitmap)
                }
            }else->{
             Log.d(TAG,"no handler onActivityReenter")
            }
        }
    }

    private fun displayImage(bitmap:Bitmap)
    {
        imageView.setImageBitmap(bitmap)
    }
}
